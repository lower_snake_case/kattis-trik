fn main() {
    let mut moves: String = String::new();
    let mut position: u8 = 1;

    std::io::stdin().read_line(&mut moves).unwrap();

    for m in moves.trim().chars() {
        match m {
            'A' => {
                if position == 1 || position == 2 {
                    position = if position == 1 { 2 } else { 1 };
                }
            }
            'B' => {
                if position == 2 || position == 3 {
                    position = if position == 2 { 3 } else { 2 };
                }
            }
            'C' => {
                if position == 1 || position == 3 {
                    position = if position == 1 { 3 } else { 1 };
                }
            }
            _ => println!("error"),
        }
    }

    println!("{}", position);
}
